Instruction how to fork blank

1. Visit your gitlab account.
2. Click the "Groups" button in the left top corner.
3. Click the green "New group"button
4. Enter group_path field like "ii_ivanov_csc_cpp_hometask_1".
5. Set visibility level to public
6. Click the green "Create group" button
7. Visit https://gitlab.com/ivafanas/csc_blank_task page. Click the white "Fork" button.
8. Choose the "ii_ivanov_csc_cpp_hometask_1" group.
9. Wait until gitlab fork is finished.
10. gitlab proceeds to your project page.
11. Find out project link at screen center. Something like a "https://gitlab.com/ii_ivanov_csc_cpp_hometask_1/csc_blank_task.git". Copy this url to clipboard.
12. Do the further actions on your local machine from the command line:

```
# clone repo and download googletests
git clone https://gitlab.com/ii_ivanov_csc_cpp_hometask_1/csc_blank_task.git
cd csc_blank_task
git submodule update --init --recursive

# make build directory
mkdir build
cd build

# generate projects
cmake ..

# build exe and unittests
cmake --build .
```

13. Implement, add to staging using `git add`, commit changes using `git commit`,
    push changes to server using `git push origin master`.
	
	
Further notes:
* Blank is tested on windows (msvc) and linux (gcc)
* visual studio, linux, gcc, clang are free projects, use them,
* If you use mingw or something else, it is on your own.
